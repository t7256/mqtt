# python 3.6

import random
import time
import json

from datetime import datetime
from paho.mqtt import client as mqtt_client
import sys


#Erik Albinssons Macbook
broker = f'{sys.argv[1]}'
port = 1883
topic = "lassemajas/temperature"
# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 1000)}'

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

def generate_temp():
    temp = random.randint(10, 35)
    date = str(datetime.now())
    temp_data = {
        "temp": temp,
        "date": date
    }
    return json.dumps(temp_data)


def publish(client):
    while True:
        temp_data = generate_temp()
        result = client.publish(topic, temp_data)
        # result: [0, 1]
        status = result[0]
        if status == 0:
            print(f"Send `{temp_data}` to topic `{topic}`")
        else:
            print(f"Failed to send message to topic {topic}")
        time.sleep(15)

def run():
    client = connect_mqtt()
    client.loop_start()
    publish(client)


if __name__ == '__main__':
    run()
